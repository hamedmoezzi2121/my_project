package main
import (
	"net/http"
	"os"
	"text/template"
	"bufio"
	"strings"
	"viewmodels"
)
func main() {
	templates := populateTemplates()
	http.HandleFunc("/",
		func (w http.ResponseWriter, req *http.Request) {
			requestedFile := req.URL.Path[1:]
			template :=
				templates.Lookup(requestedFile + ".html")
				var context interface{} = nil
				switch requestedFile {
				case "home":
					context = viewmodels.GetHome()
				case "categories":
					context = viewmodels.GetCategories()
				}
				if template != nil {
					template.Execute(w, context)
				} else {
					w.WriteHeader(404)
				}
			})
	http.HandleFunc("/css/", serveResource)
	http.HandleFunc("/img/", serveResource)
	http.ListenAndServe(":8888", nil)
}
func serveResource(w http.ResponseWriter, req *http.Request) {
	path := "/home/hamed/go/public" + req.URL.Path
	var contentType string
	if strings.HasSuffix(path, ".css") {
		contentType = "text/css"
	} else if strings.HasSuffix(path, ".png") {
		contentType = "image/png"
	} else {
		contentType = "text/plain"
	}
	f, err := os.Open(path)

	if err == nil {
		defer f.Close()
		w.Header().Add("Content-Type", contentType)
		br := bufio.NewReader(f)
		br.WriteTo(w)
	} else {
		w.WriteHeader(404)
	}
}
func populateTemplates() *template.Template {
	result := template.New("templates")
	basePath := "/home/hamed/go/templates"
	templateFolder, _ := os.Open(basePath)
	defer templateFolder.Close()
	templatePathRaw, _ := templateFolder.Readdir(-1)
	templatePaths := new([]string)
	for _, pathInfo := range templatePathRaw {
		if !pathInfo.IsDir() {
			*templatePaths = append(*templatePaths, basePath + "/" + pathInfo.Name())
		}
	}
	result.ParseFiles(*templatePaths...)
	return result
}
